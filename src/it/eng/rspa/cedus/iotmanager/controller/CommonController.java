package it.eng.rspa.cedus.iotmanager.controller;

import it.eng.iot.servlet.AjaxHandler;
import it.eng.iot.servlet.model.Permission;
import it.eng.iot.servlet.model.TokenBean;
import it.eng.iot.servlet.model.UserInfoBean;
import it.eng.iot.utils.AuthTokenManager;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.*;

public abstract class CommonController {
	
	private static final Logger LOGGER = Logger.getLogger(AjaxHandler.class.getName() );

	public static void doGet(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		HttpSession session = request.getSession();
		
		if(request.getParameter("lang")!=null){
			session.setAttribute("lang", request.getParameter("lang"));
		}
		else if ( request.getParameter("lang")==null && session.getAttribute("lang")==null ){ 
			session.setAttribute("lang", "en");
		}

		String token = (String)session.getAttribute("token");
		String refresh_token = (String)session.getAttribute("refresh_token");
		
		AuthTokenManager tokenmanager = new AuthTokenManager();
		boolean isvalidtoken = tokenmanager.checkToken(token);
		
		if(!isvalidtoken){
			TokenBean tokenBean = tokenmanager.refreshToken(refresh_token);
			isvalidtoken = tokenmanager.checkToken(tokenBean.getToken());
			if(!isvalidtoken){
				throw new Exception("Invalid Token");
			}	
		}
		
		String username = tokenmanager.getUserInfo().getDisplayName();
		request.setAttribute("username", username);
		
		// VERIFY THE USER PERMISSIONS
		Set<Permission> userPerms = null;
		try {
			userPerms = tokenmanager.getUserPermissions();
			request.setAttribute("userPerms", userPerms);
						
			// User is citizen without permission
			boolean isCitizen = false;
			if (userPerms.isEmpty()) {
				isCitizen = true;
				LOGGER.log(Level.INFO, "User has no permission then is a citizen " + isCitizen);
			}
			request.setAttribute("isCitizen", isCitizen);
			
			
			boolean userIsSeller = false;
			for (Permission perm : userPerms) {
		        
		        if (perm.getApplicationRole().equalsIgnoreCase("seller")){	
		        	//LOGGER.log(Level.INFO, "Application user role: " + perm.getApplicationRole());
		        	userIsSeller=true;
				}
		     }
			session.setAttribute("userIsSeller", userIsSeller);

			UserInfoBean userInfo = tokenmanager.getUserInfo();
			session.setAttribute("userInfo", userInfo);
			request.setAttribute("userId", userInfo.getId());
			LOGGER.log(Level.INFO, "user Id utente connesso: " + userInfo.getId());
			request.setAttribute("organization", userInfo.getOrganizations());
		}
		catch (Exception ex){
			ex.printStackTrace();
			LOGGER.log(Level.INFO, "Invalid permission");
			response.setStatus(500);
		}
	}
	
}
