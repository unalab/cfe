package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;
import java.util.Base64;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.servlet.AjaxHandler;

import java.util.logging.*;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/index")
public class IndexController extends HttpServlet {
       
	private static final long serialVersionUID = 7591147451345558646L;
	private static final Logger LOGGER = Logger.getLogger(AjaxHandler.class.getName() );
	
	public IndexController() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{ CommonController.doGet(request, response); }
		catch(Exception e){
			
			String postloginurl = Boolean.parseBoolean(Conf.getString("environment.production")) ? ConfIDM.getString("cedusdevice.host") : ConfIDM.getString("cedusdevice.localhost"); //"http://localhost:8080";
			postloginurl += request.getContextPath() + ConfIDM.getString("cedusdevice.login");
			LOGGER.log(Level.INFO, postloginurl);
			
			String state = new String(Base64.getEncoder().encode(postloginurl.getBytes()));
			
			String redirectTo = ConfIDM.getString("idm.fe.host")+"/oauth2/authorize/?"
					+ "response_type=code"
					+ "&client_id=" + ConfIDM.getString("idm.client.id") 
					+ "&state=" + state
					+ "&redirect_uri=" + ConfIDM.getString("idm.landing.page");
			
			LOGGER.log(Level.INFO, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
		}
		
		boolean isCitizen = (Boolean)(request.getAttribute("isCitizen"));
		LOGGER.log(Level.INFO, "isCitizen: " + isCitizen);
		if (isCitizen) {
			LOGGER.log(Level.INFO, "USER NOT AUTORIZED ");
			String nextJSP = "/WEB-INF/view/citizenMessage.jsp";
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
			dispatcher.forward(request,response);
		} else {
			String nextJSP = "/WEB-INF/view/index.jsp";
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
			dispatcher.forward(request,response);
		}
		
		
	}
	
	
			

}
