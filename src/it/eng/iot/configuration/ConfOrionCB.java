package it.eng.iot.configuration;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ConfOrionCB {
	private static final String BUNDLE_NAME = "it.eng.iot.configuration.configuration_orion"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private ConfOrionCB() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
