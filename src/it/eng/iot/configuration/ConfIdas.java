package it.eng.iot.configuration;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ConfIdas {
	private static final String BUNDLE_NAME = "it.eng.iot.configuration.configuration_idas"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private ConfIdas() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
