package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.core.util.Base64;

import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfGrayLogger;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.configuration.ConfOrionCB;
import it.eng.iot.servlet.model.MapCenter;
import it.eng.iot.servlet.model.Organization;
import it.eng.iot.servlet.model.RoleBean;
//import it.eng.rspa.log.logger.GraylogLogger;
//import it.eng.rspa.log.logger.model.Tool;
import it.eng.tools.Orion;
import it.eng.tools.model.EntityAttribute;
import it.eng.tools.model.ServiceEntityBean;
import it.eng.tools.model.OrganizationConfigEntity;

public abstract class ServiceConfigManager {
	
	private static final Logger LOGGER = Logger.getLogger(ServiceConfigManager.class.getName() );
	/*private static final GraylogLogger gLogger = new GraylogLogger(ConfGrayLogger.getString("GraylogClient.protocol"), 
																   ConfGrayLogger.getString("GraylogClient.host"), 
																   Integer.parseInt(ConfGrayLogger.getString("GraylogClient.port")), 
																   ConfGrayLogger.getString("GraylogClient.path"));*/
	private static Orion orion;
		
	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static String createServiceConfiguration(ServiceEntityBean serviceConfig) throws Exception {
		
		HashSet<ServiceEntityBean> serviceConfigs = new HashSet<ServiceEntityBean>();
									serviceConfigs.add(serviceConfig);
		
		JSONObject out = orion.postEntities(ConfOrionCB.getString("orion.headers.service"), ConfOrionCB.getString("orion.headers.servicepath"), serviceConfigs);
		LOGGER.log(Level.INFO,"Created service: " + out.toString());
		return out.toString();
	}
	
	
	public static Set<ServiceEntityBean> getServiceConfigList(){
		
		Set<ServiceEntityBean> out = new HashSet<ServiceEntityBean>();
		
		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		
		String resp =  orion.getEntities(headerService, headerServicePath);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		out = new Gson().fromJson(resp, type);
		
		return out;
	}
	
	
	
	public static Set<ServiceEntityBean> getServices(){
		
		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		
		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "scope"));
		
		String resp =  orion.getEntities(headerService, headerServicePath, params);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		Set<ServiceEntityBean> apiResp = new Gson().fromJson(resp, type);
		
		return apiResp;
	}
	
	public static boolean createUrbanServices(String scope, String service, String cockpitname) {
		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		EntityAttribute<String> dashboardId = (cockpitname != null) ? new EntityAttribute<String>(cockpitname) : new EntityAttribute<String>("");
		
		String entityString = "";
		
		//gLogger.urbanserviceCreated("New urbanservice created", "dashboard.s4c.eng.it", Tool.CFE, service, scope);
		
		Date date = new Date();
		List<String> dashboardIdList = new ArrayList<String>();
		dashboardIdList.add((cockpitname != null) ? cockpitname : "");
		
		
		ServiceEntityBean serviceEntity = new ServiceEntityBean("urbanservice",
																null, //service
																null, //subservice
																null, // entity_type
																new EntityAttribute<String>(CommonUtils.randomAlphanumericString(15)), //api_key
																null, // resource
																null, //map_center
																new EntityAttribute<String>(service), //name
																new EntityAttribute<Date>(date), //dateModified
																new EntityAttribute<String>("created"), //opType
																new EntityAttribute<String>(scope), //refScope
																new EntityAttribute<List<String>>(dashboardIdList) //DashBoardId
																);
		serviceEntity.setId(scope+"_"+service);
		entityString = new Gson().toJson(serviceEntity);
		JSONObject entity = new JSONObject(entityString);
		boolean result = orion.postEntity(headerService, headerServicePath, entity);
		return result;
	}
	
	/* Return the scope name */
	public static String getServiceName(String id){
		
		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		String serviceName = "";
		
		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "scope"));
			params.add(new BasicNameValuePair("id", id));
		
		String resp =  orion.getEntities(headerService, headerServicePath, params);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		Set<ServiceEntityBean> apiResp = new Gson().fromJson(resp, type);
		
		for (ServiceEntityBean serv : apiResp){
			serviceName = serv.getName().getValue();
		}
		LOGGER.log(Level.INFO,"Scope: " + id + " - " + serviceName);
		return serviceName;
	}



	public static Set<ServiceEntityBean> getServicePaths(String currService) {

		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		
		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "urbanservice"));
			params.add(new BasicNameValuePair("q", "refScope=='"+currService+"'"));
		
		String resp =  orion.getEntities(headerService, headerServicePath, params);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		Set<ServiceEntityBean> apiResp = new Gson().fromJson(resp, type);
		
		
		return apiResp;
	}
	
	/* Return the urbanservice name */
	public static String getServicePathName(String urbanserviceId, String refScope){
		
		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		String serviceName = "";
		
		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "urbanservice"));
			params.add(new BasicNameValuePair("id", urbanserviceId.replace("/", "")));
			//params.add(new BasicNameValuePair("refScope", refScope));
			params.add(new BasicNameValuePair("q", "refScope=='"+refScope+"'"));
			// TODO: CORREGGERE!!! q=refScope==refScope
		
		String resp =  orion.getEntities(headerService, headerServicePath, params);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		Set<ServiceEntityBean> apiResp = new Gson().fromJson(resp, type);
		
		for (ServiceEntityBean serv : apiResp){
			serviceName = serv.getName().getValue();
		}
		LOGGER.log(Level.INFO,"Urbanservice: " + urbanserviceId + " - " + serviceName);
		return serviceName;
	}
	
	
	
	/* Get apikey
	 * retrieves value from cedus configuration entities */
	public static String getApikey(String serviceId, String servicepathId) {

		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		String apikey = "";
		
		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "urbanservice"));
		params.add(new BasicNameValuePair("id", servicepathId.replace("/", "")));
		params.add(new BasicNameValuePair("q", "refScope=='"+serviceId+"'"));
	
		String resp =  orion.getEntities(headerService, headerServicePath, params);
		
		Type type = new TypeToken<Set<ServiceEntityBean>>(){}.getType();
		Set<ServiceEntityBean> apiResp = new Gson().fromJson(resp, type);
		
		for (ServiceEntityBean serv : apiResp){
			apikey = serv.getApikey().getValue();
		}
		LOGGER.log(Level.INFO,"apikey: " + apikey );
	
		return apikey;
	}
	
	
	/* Get apikey
	 * retrieves value from cedus configuration organization entities */
	public static String getApikey(String organizationId) {

		String headerService = ConfOrionCB.getString("orion.headers.service");
		String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
		String apikey = "";
		
		String resp =  orion.getEntity(headerService, headerServicePath, organizationId);
		
		Type type = new TypeToken<OrganizationConfigEntity>(){}.getType();
		OrganizationConfigEntity org = new Gson().fromJson(resp, type);
		apikey = org.getApikey().getValue();
	
		return apikey;
	}
	
/** 
 * Allows to get the scopes permitted 
 * according to the configured roles internal to the user organization
 * as defined into IDM and containing the configured string 
 * filterscope.convention.string set into the IDM configuration properties file 
 */	
public static Set<ServiceEntityBean> getPermittedServices(Set<ServiceEntityBean> services, Set<Organization> orgs) {

Set<ServiceEntityBean> serviceSubset = new HashSet<ServiceEntityBean>();
String scopeConventionString = ConfIDM.getString("filterscope.convention.string").trim().toLowerCase();

if (!scopeConventionString.isEmpty()){
	for (Organization org : orgs){
		Set<RoleBean> roles = org.getRoles();
		for (RoleBean role: roles){
			String roleName = role.getName();
			 if (roleName.toLowerCase().contains(scopeConventionString)){
				// turn only the specific scope
				for (ServiceEntityBean serv : services) {
					if (serv.getId().equalsIgnoreCase(roleName.replace(scopeConventionString, ""))){
						serviceSubset.add(serv);
						break;
					}
				}
				//services.retainAll(serviceSubset);
	    	 }
			 
		}
	}
	services.retainAll(serviceSubset);
}

	LOGGER.log(Level.INFO,"==================================");
	LOGGER.log(Level.INFO,"Number of permitted scopes: " + services.size());
	return services;
}
	


public static void main(String[] args) throws Exception {
	
	getApikey("bologna_1", "/bologna_1_tourism");
		
	}


public static Set<String> getCockpitName(String urbanService) {
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	
	String result = orion.getEntity(headerService, headerServicePath, urbanService);
	JSONObject entity = new JSONObject(result);
	
	JSONArray dashboards = entity.getJSONObject("dashboardid").getJSONArray("value");
	Set<String> dashboardSet = new HashSet<String>();
	
	for(int i=0;i<dashboards.length();i++) {
		dashboardSet.add(dashboards.getString(i));
	}
	
	return dashboardSet;
	
}


public static Boolean setCockpitName(String entityId, String cockpitName) {
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	System.out.println("Entity ID: "+entityId+" cockpitName: "+cockpitName);
	Set<String> cockpitSet = new HashSet<String>();
	cockpitSet.add(cockpitName);
	Boolean result = orion.updateEntityAttribute(headerService, 
												headerServicePath, 
												entityId, 
												"dashboardid", 
												cockpitSet);
	System.out.println("Result from Orion: "+result);
	return result;
}

public static Boolean appendCockpitName(String entityId, String cockpitName) {
	
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	
	String result = orion.getEntity(headerService, headerServicePath, entityId);
	JSONObject entity = new JSONObject(result);
	
	JSONArray dashboards = entity.getJSONObject("dashboardid").getJSONArray("value");
	List<String> dashboardSet = new ArrayList<String>();
	
	for(int i=0;i<dashboards.length();i++) {
		dashboardSet.add(dashboards.getString(i));
	}
	
	if(!dashboardSet.contains(cockpitName))
		dashboardSet.add(cockpitName);
	
	Boolean updateResult = orion.updateEntityAttribute(headerService, 
												headerServicePath, 
												entityId, 
												"dashboardid", 
												dashboardSet);
	
	System.out.println("Result from Orion: "+result);
	return updateResult;
}

public static Boolean removeCockpitName(String entityId, String cockpitName) {
	
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	
	String result = orion.getEntity(headerService, headerServicePath, entityId);
	JSONObject entity = new JSONObject(result);
	
	JSONArray dashboards = entity.getJSONObject("dashboardid").getJSONArray("value");
	Set<String> dashboardSet = new HashSet<String>();
	
	for(int i=0;i<dashboards.length();i++) {
		dashboardSet.add(dashboards.getString(i));
	}
	
	if(dashboardSet.contains(cockpitName))
		dashboardSet.remove(cockpitName);
	
	Boolean updateResult = orion.updateEntityAttribute(headerService, 
												headerServicePath, 
												entityId, 
												"dashboardid", 
												dashboardSet);
	
	System.out.println("Result from Orion: "+result);
	return updateResult;
}


public static Boolean deleteDashboard(String entityId) {
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	Boolean result;
	try {
		result = orion.deleteEntity(headerService, headerServicePath, entityId);
	} catch (Exception e) {
		result = false;
		e.printStackTrace();
	}
	return result;
}


public static boolean createScope(String scopeName) {
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	scopeName = scopeName.trim().toLowerCase();
	
	//gLogger.scopeCreated("New scope created", "dashboard.s4c.eng.it", Tool.CFE, scopeName);
	
	Date date = new Date();
	MapCenter mapcenter = null;
	EntityAttribute<MapCenter> mapCenterEntity = null;
	try {
		mapcenter = getGoogleMapLocation(scopeName);
		mapCenterEntity = new EntityAttribute<MapCenter>(mapcenter);
	} catch (Exception e) {		
		LOGGER.log(Level.WARNING, "Impossible to find map center of " + scopeName);
		LOGGER.log(Level.WARNING, e.getMessage());
	}
	ServiceEntityBean serviceEntity = new ServiceEntityBean("scope",
															null, //service
															null, //subservice
															null, // entity_type
															null, //api_key
															null, // resource
															mapCenterEntity, //map_center
															new EntityAttribute<String>(StringUtils.capitalize(scopeName)), //name
															new EntityAttribute<Date>(date), //dateModified
															new EntityAttribute<String>("created"), //opType
															null, //refScope
															null //DashBoardId
															);
	serviceEntity.setId(scopeName);
	String entityString = new Gson().toJson(serviceEntity);
	JSONObject entity = new JSONObject(entityString);
	boolean result = orion.postEntity(headerService, headerServicePath, entity);
	return result;
}

public static MapCenter getGoogleMapLocation(String keyword) throws Exception{
	
	String endpoint = Conf.getString("google.maps.apis");
	String key = Conf.getString("google.maps.apikey");
	
	Set<BasicNameValuePair> queryParams = new HashSet<BasicNameValuePair>();
	queryParams.add(new BasicNameValuePair("address", keyword));
	queryParams.add(new BasicNameValuePair("key", key));
	
	String queryString = "?"+URLEncodedUtils.format(queryParams, Charset.defaultCharset());
	String url = endpoint + queryString;
	String r = RestUtils.consumeGet(url);
	
	JSONObject jsonObj = new JSONObject(r);
	JSONArray jsArray = (JSONArray) jsonObj.get("results");
	MapCenter mapcenter = new MapCenter();
	
	for (int i = 0; i < jsArray.length(); i++) {
		try {
			JSONObject record = jsArray.getJSONObject(i);
			if(record.has("geometry")){
				JSONObject geometryj = record.getJSONObject("geometry");
				
				if(geometryj.has("location")){
					JSONObject location = geometryj.getJSONObject("location");
					
					if(location.has("lat") && location.has("lng")){
						
						mapcenter = new MapCenter();
						
						mapcenter.setLat(location.getDouble("lat"));
						mapcenter.setLng(location.getDouble("lng"));
						
						if(location.has("zoom")){
							mapcenter.setZoom(location.getInt("zoom"));
						}
						
						
					}
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	return mapcenter;
}


public static Boolean deleteDashboardFromScopeName(String scope) {
	scope = scope.trim().toLowerCase();
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");
	Set<BasicNameValuePair> queryParams = new HashSet<BasicNameValuePair>();
	queryParams.add(new BasicNameValuePair("type", "urbanservice"));
	queryParams.add(new BasicNameValuePair("q", "refScope=="+scope));
	
	//Get all the dashboard from scope name
	String result = orion.getEntities(headerService, headerServicePath, queryParams );
	JSONArray dashboards = new JSONArray(result);
	String entityId;
	Boolean returnResult = true;
	try {
		for(int i=0;i< dashboards.length();i++) {
			JSONObject dashboard = dashboards.getJSONObject(i);
			entityId = dashboard.getString("id");
//			gLogger.urbanserviceDeleted("Urbanservice deleted", "dashboard.s4c.eng.it", Tool.CFE, dashboard.getJSONObject("name").getString("value"), scope);
			returnResult = orion.deleteEntity(headerService, headerServicePath, entityId);
		}		
	} catch (Exception e) {
		returnResult = false;
		e.printStackTrace();
	}
	return returnResult;
}


public static Boolean deleteScope(String scope) {
	scope = scope.trim().toLowerCase();
	String headerService = ConfOrionCB.getString("orion.headers.service");
	String headerServicePath = ConfOrionCB.getString("orion.headers.servicepath");

	//gLogger.scopeDeleted("Scope deleted", "dashboard.s4c.eng.it", Tool.CFE, scope);
	
	Boolean returnResult = true;
	try {
		returnResult = orion.deleteEntity(headerService, headerServicePath, scope);		
	} catch (Exception e) {
		returnResult = false;
		e.printStackTrace();
	}
	return returnResult;
}


public static String getAllDocuments(String username,String password) {
	String url = Conf.getString("knowage.url")+"/restful-services/2.0/documents";
	LOGGER.log(Level.INFO, "Invoking url: " + url);
	Map<String, String> headers = new HashMap<String, String>();
	byte[] b64auth = Base64.encode(username+":"+password);
		headers.put("Authorization","Basic "+new String(b64auth));
	String resp = "";
	try{ resp = RestUtils.consumeGet(url, headers); }
	catch(Exception e){
		e.printStackTrace();
	}
	return resp;
}


	
}
