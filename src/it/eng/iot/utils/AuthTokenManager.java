package it.eng.iot.utils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import com.google.gson.Gson;
import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.servlet.model.Organization;
import it.eng.iot.servlet.model.OrganizationInternalRole;
import it.eng.iot.servlet.model.OrganizationRole;
import it.eng.iot.servlet.model.Permission;
import it.eng.iot.servlet.model.RoleBean;
import it.eng.iot.servlet.model.TokenBean;
import it.eng.iot.servlet.model.UserInfoBean;
import it.eng.tools.Keyrock;
import it.eng.tools.model.KeyrockToken;
import it.eng.tools.model.Resources;
import java.util.logging.*;

public class AuthTokenManager{
	
	private UserInfoBean userinfo;
	private static final Logger LOGGER = Logger.getLogger(AuthTokenManager.class.getName() );	
	
	public boolean checkToken(String token) {
		
		boolean isvalidtoken = true;
		
		try{
			LOGGER.log(Level.INFO, "User Access Token: " + token);
			setUserInfo(getRemoteUserInfo(token));
		}
		catch(Exception e){
			setUserInfo(null);
			isvalidtoken = false;
		}
		
		return isvalidtoken;
		
	}

	public TokenBean refreshToken(String refresh_token) 
			throws ServletException, IOException {

		String client_id = ConfIDM.getString("idm.client.id");
		String client_secret = ConfIDM.getString("idm.client.secret");

		TokenBean tb = new TokenBean();

		try {
			Keyrock keyrock = new Keyrock();
			String out = keyrock.refresh_token(refresh_token, client_id, client_secret);
			
			KeyrockToken newtoken = new Gson().fromJson(out, KeyrockToken.class);
			
			Integer expires_in =newtoken.getExpires_in();
			Long currentDate = System.currentTimeMillis();
			Long validityDate = Long.sum(currentDate, expires_in);
			
			// Build the object to send to the client
			tb = new TokenBean(newtoken.getAccess_token(), newtoken.getRefresh_token(), "", String.valueOf(validityDate));

		} 
		catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
		return tb;
	}
	
	public UserInfoBean getRemoteUserInfo(String token)
			throws Exception {
		
		Keyrock keyrock = new Keyrock();

		String j_userinfo = keyrock.getUserInfo(token);
		LOGGER.log(Level.INFO, "/user responded: " + j_userinfo);
		return new Gson().fromJson(j_userinfo, UserInfoBean.class);

	}
	
	public UserInfoBean getUserInfo(){
		return this.userinfo;
	}
	
	public void setUserInfo(UserInfoBean info){
		this.userinfo = info;
	}
	
	public Set<Permission> getUserPermissions (){
		
		// 1. Load configuration permission
		LOGGER.log(Level.INFO, "=== getUserPermissions ===");
		
		Permission userPerm;
		Set<Permission> userPermSet = new HashSet<Permission>();
		Set<Permission> configPermissions = loadConfigPermissions();
		/*
		for (Permission perm : configPermissions) {
			if (perm.getApplicationRole().equalsIgnoreCase("seller")){
				LOGGER.log(Level.INFO, "SELLER Configuration: " + perm.getPermissionCRUD());
			}
			
		}
		*/
		
	
		// 2. ROLE AND PERMISSION OF THE CONNECTED USER
		String userId = userinfo.getId();
		LOGGER.log(Level.INFO, "CONNECTED USER ID: " + userId);
		// 2.1 GET RUOLI ESTERNI
		/*
		Set<RoleBean> userRoles = userinfo.getRoles();
		for (RoleBean userRole : userRoles) {
			LOGGER.log(Level.INFO, "Connected user EXTERNAL ROLE role: " + userRole.getName());
		}
		*/
		
		// 2.2. RUOLI INTERNI ALL'ORGANIZZAZIONE
		Set<Organization> orgs = userinfo.getOrganizations();
		LOGGER.log(Level.INFO, "Number of user organization: " + userinfo.getOrganizations().size());
		for (Organization org : orgs) {
			LOGGER.log(Level.INFO, "Organization >>> " + org.getName() + " | " + org.getId());
			String organizationId = org.getId();
			Set<RoleBean> userOrgRoles = org.getRoles();
			
			
			for (RoleBean orgUserRole : userOrgRoles) {
				LOGGER.log(Level.INFO, "Connected user organization role: " + orgUserRole.getName());
				
				OrganizationRole roles = null; //???
				String userOrganizationRole = null;
				try {
					roles = getUserOrganizationRole(organizationId, userId);
					userOrganizationRole = roles.getRoles().iterator().next().getName();
					LOGGER.log(Level.INFO, "User: " + userId + " OrganizationId: " + organizationId + " User Organization Role: " + userOrganizationRole );
				} catch (Exception e) {
					e.printStackTrace();
					userOrganizationRole="member";
				}
				
						
				String roleName = orgUserRole.getName().trim().replaceAll("\\s","").toLowerCase();
				// GET THE CORRESPONDING PERMISSION
				for (Permission perm : configPermissions) {
					//String assetRole = "owner"; // GET FROM API
					if (perm.getApplicationRole().equalsIgnoreCase(roleName) && perm.getOrganizationsRole().equalsIgnoreCase(userOrganizationRole)){
						userPerm = new Permission(); // svuoto
						userPerm.setPermissionCRUD(perm.getPermissionCRUD());
						userPerm.setApplicationRole(perm.getApplicationRole());
						userPerm.setOrganizationsRole(perm.getOrganizationsRole());
						userPerm.setAsset(perm.getAsset());
						userPerm.setAssetRole(perm.getAssetRole());
						LOGGER.log(Level.INFO, "user role: " + roleName + " permissions " + userPerm.getPermissionCRUD() + " " + userPerm.getAsset() + " is asset owner: " + userPerm.getAssetRole());
						// LOAD THE Permission IN THE PERMISSIONS
						userPermSet.add(userPerm);
					}
					
				}
				
				LOGGER.log(Level.INFO, "user role: " + roleName);
				LOGGER.log(Level.INFO, "userPerms: " + userPermSet);	
				
				
			}
		}
		// RETURN TO THE FRONT END THE CONNECTED USER PERMISSION
		//LOGGER.log(Level.INFO, "Number of permission returned: " + userPermSet.size());
		return userPermSet;
	
	}

	/*
	 *  Loads configuration permissions
	 */
	public Set<Permission> loadConfigPermissions (){
		
		 Set<Permission> configPermissions = new HashSet<Permission>();
		 
		 // Seller
		 // CASE: seller.owner.scope.owner=CRUD
		 Permission permission = new Permission();
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.owner.scope.owner"));
		 configPermissions.add(permission);
		 
		 // CASE: seller.owner.urbanservice.owner=CRUD
		 permission = new Permission();
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.owner.urbanservice.owner"));
		 configPermissions.add(permission);
		 
		 //CASE: seller.owner.device.owner=CRUD
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.owner.device.owner"));
		 configPermissions.add(permission);	
		 
		 // FIX 
		 // Seller - member
		 // CASE: seller.member.scope.owner=CRUD
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.member.scope.owner"));
		 configPermissions.add(permission);
		 
		 // CASE: seller.member.urbanservice.owner=CRUD
		 permission = new Permission();
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.member.urbanservice.owner"));
		 configPermissions.add(permission);
		 
		 //CASE: seller.member.device.owner=CRUD
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("seller");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("seller.member.device.owner"));
		 configPermissions.add(permission);	
		 
		 // ********************************
		 //#cityManager OWNER
		 // CASE: citymanager.owner.scope.owner=CRUD
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.owner.scope.owner"));
		 configPermissions.add(permission);	
		 
		 
		 // CASE: citymanager.owner.urbanservice.owner=CRUD
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.owner.urbanservice.owner"));
		 configPermissions.add(permission);	
				 
				 
		 // CASE: citymanager.owner.device.owner=CRUD
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.owner.device.owner"));
		 configPermissions.add(permission);	
		 
		 
		//#cityManager MEMBER and Asset OWNER
		 // CASE: citymanager.member.scope.owner=CRUD
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.scope.owner"));
		 configPermissions.add(permission);	
		 
		 // CASE: citymanager.member.urbanservice.owner=CRUD
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.urbanservice.owner"));
		 configPermissions.add(permission);	
		 
		 // CASE: citymanager.member.device.owner=CRUD
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.device.owner"));
		 configPermissions.add(permission);	
		 
		 //#cityManager MEMBER and Asset NOT OWNER
		 // CASE: citymanager.member.scope.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("scope");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.scope.member"));
		 configPermissions.add(permission);	
		 
		 // CASE: citymanager.member.urbanservice.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.urbanservice.member"));
		 configPermissions.add(permission);
		 
		 // CASE: citymanager.member.device.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("citymanager");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("device");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("citymanager.member.device.member"));
		 configPermissions.add(permission);
		 
		 
		 // ********************************
		 //#urbanserviceProvider OWNER
		 // CASE: urbanserviceprovider.owner.scope.owner=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.scope.owner"));
		 configPermissions.add(permission);	
		 
		 
		 // CASE: urbanserviceprovider.owner.urbanservice.owner=R
		 permission = new Permission(); // svuoto
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.urbanservice.owner"));
		 configPermissions.add(permission);	
				 
				 
		 // CASE: urbanserviceprovider.owner.device.owner=CRUD
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("owner");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.device.owner"));
		 configPermissions.add(permission);	
		 
		 
		//#cityManager MEMBER and Asset OWNER
		 // CASE: urbanserviceprovider.member.scope.owner=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("scope");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.scope.owner"));
		 configPermissions.add(permission);	
		 
		 // CASE: urbanserviceprovider.member.urbanservice.owner=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.urbanservice.owner"));
		 configPermissions.add(permission);	
		 
		 // CASE: urbanserviceprovider.member.device.owner=CRU
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("device");
		 permission.setAssetRole("owner");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.device.owner"));
		 configPermissions.add(permission);	
		 
		 //#cityManager MEMBER and Asset NOT OWNER
		 // CASE: urbanserviceprovider.member.scope.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("scope");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.scope.member"));
		 configPermissions.add(permission);	
		 
		 // CASE: urbanserviceprovider.member.urbanservice.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("urbanservice");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.urbanservice.member"));
		 configPermissions.add(permission);
		 
		 // CASE: urbanserviceprovider.member.device.member=R
		 permission = new Permission(); 
		 permission.setApplicationRole("urbanserviceprovider");
		 permission.setOrganizationsRole("member");
		 permission.setAsset("device");
		 permission.setAssetRole("member");
		 permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.device.member"));
		 configPermissions.add(permission);
		 
		 LOGGER.log(Level.INFO, "Loaded configured permissions " + configPermissions.size());
		 return configPermissions;
		 
	};
	
	
	
	public static OrganizationRole getUserOrganizationRole(String organizationId, String userId)
			throws Exception {
		
		Keyrock keyrock = new Keyrock();
		OrganizationRole userOrganizationRole = keyrock.getUserOrganizationRole(organizationId, userId);
		
		return userOrganizationRole;
	}
	
	
	/** Get all the active organizations */
	public static Set<Resources> getAllActiveOrganizations()
			throws Exception {
		
		Keyrock keyrock = new Keyrock();
		Set<Resources> organizations = keyrock.getAllActiveOrganizations();
		
		return organizations;
	}
	
	
	
	/** Get all the users having a specific roleName (owner or member) in the organization */
	public static Set<Resources> getOrganizationUsers(String organizationId, String roleName)
			throws Exception {
		
		Keyrock keyrock = new Keyrock();
		Set<Resources> users = keyrock.getOrganizationActiveUsers(organizationId);
	
		for (Resources user : users){
			String userId = user.getId();
			
			OrganizationRole userOrganizationRole = getUserOrganizationRole(organizationId, userId);
			Set<OrganizationInternalRole> roles = userOrganizationRole.getRoles();
			for (OrganizationInternalRole role: roles){
				if (!role.getName().equalsIgnoreCase(roleName)){
					users.remove(user);
				}
				
			}
			
		}
		LOGGER.log(Level.INFO, "Returned users: " + users.size() + " having role name: " + roleName);
		return users;
	}
	
	
		
	
	
	public static void main(String [ ] args){
		try {
			//String adminToken = Keyrock.getAdminToken();
			//LOGGER.log(Level.INFO, adminToken);
			/*
			String organizationId = "cef31912da3a40668cda0cf89b6fc0a4";
			String userId = "gambone";
			OrganizationRole roles = getUserOrganizationRole(organizationId, userId);
			String organizationRole = roles.getRoles().iterator().next().getName();
			LOGGER.log(Level.INFO, "=================== " + organizationRole);
			*/
			
			
			Set<Resources> organizations = getAllActiveOrganizations();
			LOGGER.log(Level.INFO, "=======================");
			for (Resources org : organizations) {
				org.getId();
				org.getName();
				org.getActive();
				LOGGER.log(Level.INFO, org.getId() + " - " + org.getName() + " - " + org.getActive());
			}
			LOGGER.log(Level.INFO, "======================= END");
			
			
			/*
			String organizationId = "df298fb6023b4a54837fc93040932638";
			String roleName = "owner";
			//String roleName = "member";
			Set<Resources> users = getOrganizationUsers(organizationId, roleName);
			for (Resources user : users) {
				user.getId();
				user.getName();
				user.getUrn().getDefault_project_id();
				LOGGER.log(Level.INFO, user.getId() + " - " + user.getName() + " - " + user.getUrn().getDefault_project_id());
			}
			*/
			LOGGER.log(Level.INFO, "===================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
}
