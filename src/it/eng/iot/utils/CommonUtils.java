package it.eng.iot.utils;

import java.net.URL;
import java.security.SecureRandom;

public abstract class CommonUtils {

	public static boolean isValidURL(String urlString){
	    boolean out = false;
		try{
	        URL url = new URL(urlString);
	        url.toURI();
	        out = true;
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}
	
	public static String randomAlphanumericString(int length) {
		SecureRandom rnd = new SecureRandom();
		String values = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuilder sb = new StringBuilder(length);
		   for( int i = 0; i < length; i++ ) 
		      sb.append( values.charAt( rnd.nextInt(values.length()) ) );
		return sb.toString();
	}
	
}
