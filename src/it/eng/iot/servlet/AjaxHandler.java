package it.eng.iot.servlet;



import it.eng.iot.configuration.ConfGrayLogger;
import it.eng.iot.servlet.model.Organization;
import it.eng.iot.servlet.model.TokenBean;
import it.eng.iot.servlet.model.UserInfoBean;
import it.eng.iot.utils.AuthTokenManager;
import it.eng.iot.utils.ServiceConfigManager;
//import it.eng.rspa.log.logger.GraylogLogger;
//import it.eng.rspa.log.logger.model.Tool;
import it.eng.tools.Orion;
import it.eng.tools.model.ServiceEntityBean;
import it.eng.tools.model.Resources;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Path;




import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import java.util.logging.*;



@WebServlet("/ajaxhandler")
public class AjaxHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;
	
	private static final Logger LOGGER = Logger.getLogger(AjaxHandler.class.getName() );
	/*private static final GraylogLogger gLogger = new GraylogLogger(ConfGrayLogger.getString("GraylogClient.protocol"), 
																   ConfGrayLogger.getString("GraylogClient.host"), 
																   Integer.parseInt(ConfGrayLogger.getString("GraylogClient.port")), 
																   ConfGrayLogger.getString("GraylogClient.path"));*/
	
	

		
	public enum Ajaxaction {
		get_token("get_token"), check_token("check_token"), 
		getContextsList("getContextsList"), subscribe("subscribe"),
		create_urban_service("create_urban_service"), 
		getServices("getServices"), getServicePaths("getServicePaths"),
		getServiceName("getServiceName"), getServicePathName("getServicePathName"),
		getOrganizations("getOrganizations"),
		getAllActiveOrganizations("getAllActiveOrganizations"),
		getCockpitNames("getCockpitNames"),
		setCockpitName("setCockpitName"),deleteDashboard("deleteDashboard"),
		create_scope("create_scope"),
		deleteDashboardFromScopeName("deleteDashboardFromScopeName"),
		deleteScope("deleteScope"),getAllDocuments("getAllDocuments"), 
		appendDashboard("appendDashboard"),removeDashboard("removeDashboard");

		
		private final String text;

		private Ajaxaction(final String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}

	};

	public AjaxHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Gson gson = new Gson();
		JSONObject jResp = new JSONObject();
				jResp.put("error", false);
		
		try {
			String action = request.getParameter("action"); //$NON-NLS-1$
			Ajaxaction eAction = Ajaxaction.valueOf(action);
			
			switch (eAction) {
			
				case deleteDashboardFromScopeName: {
					//Validate request					
					String scope = (request.getParameter("scope"));
					Boolean serviceResponse = ServiceConfigManager.deleteDashboardFromScopeName(scope);
					jResp.put("orion_delete", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				} 
				
				case deleteScope: {
					//Validate request					
					String scope = (request.getParameter("scope"));
					Boolean serviceResponse = ServiceConfigManager.deleteScope(scope);
					jResp.put("orion_delete", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				} 
				
				/* Create new urban service */
				case create_urban_service: {
					//Validate request
					String cockpitname = (request.getParameter("cockpitname"));
					String scope = request.getHeader("fiware-service");
					String service = request.getHeader("fiware-servicepath");
					LOGGER.log(Level.INFO, service );
					//System.out.println("CREATE_URBAN_SERVICE FIRED. scope: "+scope+" service: "+service);
					//JSONObject orionj = new JSONObject();
					boolean serviceResponse = ServiceConfigManager.createUrbanServices(scope, service,cockpitname);
					//orionj.put(serviceConfigBean.getId(), serviceResponse);
					jResp.put("orion_registration", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;					
				}
				
				case create_scope: {
					String scopeName = request.getParameter("scope");
					LOGGER.log(Level.INFO, scopeName );
					boolean serviceResponse = ServiceConfigManager.createScope(scopeName);
					jResp.put("orion_registration", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;	
					
				}
							
				case subscribe: {
					String body = request.getParameter("payload");
					String fiwareservice = request.getHeader("fiware-service");
					String fiwareservicepath = request.getHeader("fiware-servicepath");
					
					Orion orion = new Orion();
					boolean result = orion.subscribe(fiwareservice, fiwareservicepath, body);
					jResp.put("error", !result);
					
					response.getWriter().write(jResp.toString());
					break;
				}
				
				
				// Gets the list of contexts configured on Orion
				case getContextsList: {
					LOGGER.log(Level.INFO,"called getContextsList " );
					try {
						Set<ServiceEntityBean> entities = ServiceConfigManager.getServiceConfigList();
						response.getWriter().write(gson.toJson(entities));
					} 
					catch (org.json.JSONException jsone) {
						jsone.printStackTrace();
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
					break;
				}
				
				case check_token:{
					String token = request.getParameter("token");
					String refresh_token = request.getParameter("refresh_token");
					
					AuthTokenManager tokenmanager = new AuthTokenManager();
					boolean isvalidtoken = tokenmanager.checkToken(token);
					
					if(!isvalidtoken){
						TokenBean tokenBean = tokenmanager.refreshToken(refresh_token);
						
						isvalidtoken = tokenmanager.checkToken(tokenBean.getToken());
						if(!isvalidtoken){
							LOGGER.log(Level.INFO,"Invalid token");
						}
						response.setStatus(500);
					}
					UserInfoBean userinfo = tokenmanager.getUserInfo();
					
					response.getWriter().write(gson.toJson(userinfo));
				
					break;
				}
				


				case getServicePaths:{
					try {
					String currService = request.getParameter("service"); //$NON-NLS-1$
					Set<ServiceEntityBean> servicepaths = ServiceConfigManager.getServicePaths(currService);
					
					response.getWriter().write(gson.toJson(servicepaths));
					}
					catch (org.json.JSONException jsone) {
						jsone.printStackTrace();
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
					break;
				}
				
				case getServices:{
					try {
						Set<ServiceEntityBean> services = ServiceConfigManager.getServices();
						
						// Get user info 
						HttpSession session = request.getSession();
						UserInfoBean userInfo = (UserInfoBean) session.getAttribute("userInfo");
						// Verify userIsSeller 
						boolean userIsSeller= false;
						if(session.getAttribute("userIsSeller") != null){
						    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
						}
						if (!userIsSeller){		
							Set<Organization> orgs = userInfo.getOrganizations();	
							Set<ServiceEntityBean> permittedServices = ServiceConfigManager.getPermittedServices(services, orgs);
							services.retainAll(permittedServices);
						}
						response.getWriter().write(gson.toJson(services));
					}
					catch (org.json.JSONException jsone) {
						jsone.printStackTrace();
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
					break;
				}
				
				// Get the name of the scope
				case getServiceName:{
					try {
						String id = request.getParameter("id");
						String serviceName = ServiceConfigManager.getServiceName(id);
						
						response.getWriter().write(gson.toJson(serviceName));
					}
					catch (org.json.JSONException jsone) {
						jsone.printStackTrace();
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
					break;
				}
				// Get the name of the urbanservice
				case getServicePathName:{
					try {
						String refScope = request.getParameter("service");
						String urbanserviceId = request.getParameter("servicepath");
						String servicePathName = ServiceConfigManager.getServicePathName(urbanserviceId, refScope);
						
						response.getWriter().write(gson.toJson(servicePathName));
					}
					catch (org.json.JSONException jsone) {
						jsone.printStackTrace();
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
					break;
				}
			
					
			
			// Gets Organization
			case getOrganizations: {
				LOGGER.log(Level.INFO,"called getOrganizations " );
				try {
					Set<ServiceEntityBean> entities = ServiceConfigManager.getServiceConfigList();
					response.getWriter().write(gson.toJson(entities));
				} 
				catch (org.json.JSONException jsone) {
					jsone.printStackTrace();
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(500);
				}
				break;
			}
			
			// Gets All the Active Organizations
			case getAllActiveOrganizations: {
				LOGGER.log(Level.INFO,"Called getAllActiveOrganizations " );
				try {
					Set<Resources> allOrgs = AuthTokenManager.getAllActiveOrganizations();
					response.getWriter().write(gson.toJson(allOrgs));
				} 
				catch (org.json.JSONException jsone) {
					jsone.printStackTrace();
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(500);
				}
				break;
			}
			
				case getCockpitNames : {
					//Validate request
					
					String urbanService = (request.getParameter("urbanService"));
					
					Set<String> serviceResponseSet = ServiceConfigManager.getCockpitName(urbanService);
//					String serviceResponse = null;
//					for (Iterator<String> it = serviceResponseSet.iterator(); it.hasNext(); ) {
//				        serviceResponse = it.next();
//				        	break;
//				    }
					JSONArray jsonResp = new JSONArray(serviceResponseSet);
					response.getWriter().write(jsonResp.toString());
					break;	
				}
				
				case setCockpitName : {
					//Validate request					
					String entityId = (request.getParameter("entityId"));
					String cockpitName = (request.getParameter("cockpitName"));
					Boolean serviceResponse = ServiceConfigManager.setCockpitName(entityId,cockpitName);
					jResp.put("orion_update", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				}
				
				case appendDashboard : {
					//Validate request					
					String entityId = (request.getParameter("entityId"));
					String cockpitName = (request.getParameter("cockpitName"));
					Boolean serviceResponse = ServiceConfigManager.appendCockpitName(entityId, cockpitName);
					jResp.put("orion_update", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				}
				
				case removeDashboard: {
					//Validate request					
					String entityId = (request.getParameter("entityId"));
					String cockpitName = (request.getParameter("cockpitName"));
					Boolean serviceResponse = ServiceConfigManager.removeCockpitName(entityId, cockpitName);
					jResp.put("orion_update", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				}
				
				case deleteDashboard : {
					//Validate request					
					String scope = (request.getParameter("scope"));
					String dashboard = (request.getParameter("dashboard"));
					
					//gLogger.urbanserviceDeleted("Urbanservice deleted", "dashboard.s4c.eng.it", Tool.CFE, dashboard, scope);
					
					String entityId = scope + "_" + dashboard;
					Boolean serviceResponse = ServiceConfigManager.deleteDashboard(entityId);
					jResp.put("orion_update", serviceResponse);
					response.getWriter().write(jResp.toString());
					break;
				}
				
				case getAllDocuments : {
					String username = request.getParameter("username");
					String password = request.getParameter("password");
					String serviceResponse = ServiceConfigManager.getAllDocuments(username,password);
					response.getWriter().write(serviceResponse);
					break;
				}
				
			default:
					throw new Exception("Unsupported action " + action); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			
			LOGGER.log(Level.INFO,e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); //$NON-NLS-1$
			jResp.put("message", e.toString()); //$NON-NLS-1$
			
			response.getWriter().write(jResp.toString());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	

	
	
}
