package it.eng.iot.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.logging.*;

import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfGrayLogger;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.utils.AuthTokenManager;
//import it.eng.rspa.log.logger.GraylogLogger;
//import it.eng.rspa.log.logger.model.Tool;
import it.eng.tools.Keyrock;
import it.eng.tools.model.KeyrockToken;

@WebServlet(description = "login", urlPatterns = { "/login" })
public class Login extends HttpServlet {
	
	private static final long serialVersionUID = -4288687805581054812L;
	
	private static final Logger LOGGER = Logger.getLogger(Login.class.getName() );
	/*private static final GraylogLogger gLogger = new GraylogLogger(ConfGrayLogger.getString("GraylogClient.protocol"), 
																   ConfGrayLogger.getString("GraylogClient.host"), 
																   Integer.parseInt(ConfGrayLogger.getString("GraylogClient.port")), 
																   ConfGrayLogger.getString("GraylogClient.path"));*/
	
	public Login() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String code = request.getParameter("code");
		
			try {
				Keyrock keyrock = new Keyrock();
				KeyrockToken kr_resp = keyrock.getToken(code, ConfIDM.getString("idm.client.id"), ConfIDM.getString("idm.client.secret"), ConfIDM.getString("idm.landing.page"));
				
				AuthTokenManager tokenmanager = new AuthTokenManager();
								tokenmanager.checkToken(kr_resp.getAccess_token());
				
				//gLogger.login("Login to CFE", "dashboard.s4c.eng.it", Tool.CFE);
				
				HttpSession session = request.getSession(false);
							session.setAttribute("token", kr_resp.getAccess_token());
							session.setAttribute("refresh_token", kr_resp.getRefresh_token());
							
				// Load if URBO is 
				String urboIntegration = Conf.getString("urbo.integration");
				session.setAttribute("urboIntegration", urboIntegration );
							
				response.sendRedirect(request.getContextPath() + Conf.getString("login.redirect.page"));

			} 
			catch (Exception e) {
				LOGGER.log(Level.SEVERE,e.getMessage());
				response.sendRedirect(request.getContextPath() + Conf.getString("error.page"));
			}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setStatus(405);

	}


}
