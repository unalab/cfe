package it.eng.iot.servlet;

//import it.eng.rspa.log.logger.GraylogLogger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CFEServletListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		//GraylogLogger.stopLogger();
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("Starting City Front end");
	}

}
