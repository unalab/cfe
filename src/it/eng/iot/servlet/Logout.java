package it.eng.iot.servlet;

import it.eng.iot.configuration.ConfIDM;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/logout")
public class Logout extends HttpServlet {
	
	private static final long serialVersionUID = 693724303976303888L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.removeAttribute("token");
		session.removeAttribute("refresh_token");
		
		session.removeAttribute("userPerms");
		session.removeAttribute("userIsSeller");
		session.removeAttribute("isSeller");
		
		response.sendRedirect(ConfIDM.getString("idm.logout.page"));
		
	}

}
