<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms"> 
 	 <c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
 		<c:set var = "isSeller" scope = "session" value = "true"/> 
 	 </c:if> 
 	 <c:if test = "${fn:containsIgnoreCase(userPerms.asset, 'scope')}">
 		<c:set var = "scopePermission" scope = "session" value = "${userPerms.permissionCRUD}"/> 
 	 </c:if>
 	 <c:if test = "${fn:containsIgnoreCase(userPerms.asset, 'urbanservice')}">
 		<c:set var = "urbanServicePermission" scope = "session" value = "${userPerms.permissionCRUD}"/> 
 	 </c:if>
 </c:forEach> 

<%--  <td><c:out value="${urboIntegration}"></c:out></td> --%>


<html class="iotmanager">
<head>
	<title>City Enabler Front End</title>
	
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
	<script type="text/javascript" src="js/conf/knowage_conf.js"></script>
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/index.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
</head>

<body class="materialize cedus-gray">
	<%@include file="../frames/loader.jspf"%>
	
	<header>
		<%@include file="../frames/navbar.jspf"%>
	</header>
	
	<div class="container">
		<div class="row hide" id="errorMessage"></div>
	
		<h6 id="nrcat" class="row center"><b><span>0</span>&nbsp;<i18n:message value="scopes"/></b></h6>
  
		<div id="catlist">
			<%@include file="../frames/categoryitem.jspf" %>
			
			<!-- isSeller and is integrated with urbo -->
				<div class="row">
						<div class="col s12 m6 l4 catitem " id="newcattrigger">
						<c:if test = "${fn:containsIgnoreCase(scopePermission, 'C')}">							 
								<div class="hoverable z-depth-1 block cedus-gray nopadding">
									<a href="#newcatmodal"  class="catanchor center-align text-cedus-bluegray catname">
										<i class="material-icons">add</i>
										<br/>
										<span><i18n:message value="new_scope"/></span>
									</a>
								</div>
							</c:if> 
						</div>
					</div>
		
			
		</div>	
		
		<%@include file="../frames/settings_newcatmodal.jspf"%>
		
	</div>
	<iframe id="knowage-loader" class="hide" src="https://s4c.eng.it/knowage"></iframe>
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	<script type="text/javascript" src="js/model/category.js"></script>
	<%@include file="../frames/messages.jspf"%>
	
</body>

</html>
