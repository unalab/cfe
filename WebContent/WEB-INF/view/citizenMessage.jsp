<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms"> 
 	<c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
 		<c:set var = "isSeller" scope = "session" value = "true"/> 
 	 </c:if> 
 </c:forEach> 


		

<html class="iotmanager">
<head>
	<title><i18n:message value="apptitle"/></title>
	
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
	<script type="text/javascript" src="js/conf/knowage_conf.js"></script>
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/index.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
</head>

<body class="container materialize cedus-gray">
	<%@include file="../frames/loader.jspf"%>
	
	<div class="materialize cedus-gray">
      <h2 class="materialize cedus-gray"><i18n:message value="msg_citizen_message"/></h2>
      <hr/>
     
      <table>
      <thead>
         <tr><th><i18n:message value="msg_citizen_warning"/></th></tr>
      </thead>
     
      </table>
	</div>
	
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	
	<%@include file="../frames/messages.jspf"%>
	
</body>

</html>
