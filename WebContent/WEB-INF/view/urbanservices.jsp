<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms">
	<c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
		<c:set var = "isSeller" scope = "session" value = "true"/>
	 </c:if>
</c:forEach>



<html class="iotmanager">
<head>
	<title>City Enabler Front End</title>
	
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
	<script type="text/javascript" src="js/knowageSDK/sbisdk-all-production.js"></script>
	<script type="text/javascript" src="js/conf/knowage_conf.js"></script>
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/urbanservices.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
</head>

<body class="materialize cedus-gray">
	<%@include file="../frames/loader.jspf"%>
	
	<header>
		<%@include file="../frames/navbar.jspf"%>
	</header>

	<div class="container">
		
		<div class="row hide" id="errorMessage"></div>
		
		<div class="row h-40">
			<div class="col s2 m1">
				<a href="index" class="btn-floating btn-flat waves-effect waves-light left">
					<i class="text-cedus-bluegray material-icons text-cedus-darkblue">arrow_back</i>
				</a>
			</div>
			<div class="col s10 m11 breadcrumb_bar nav-wrapper valign-wrapper">
		       <a href="index" id="scope_breadcrumb" class="valign breadcrumb text-cedus-bluegray"><i18n:message value="scope"/></a>
			</div>
		</div>
		
		<div id="urbanserviceslist">
			<%@include file="../frames/urbanserviceitem.jspf" %>
			<div class="row">
					<div class="col s12 m4 l3 urbanserviceitem" id="newurbanservicetrigger">					
						<c:if test = "${fn:containsIgnoreCase(urbanServicePermission, 'C')}">
							<div class="hoverable z-depth-1 block cedus-gray nopadding" >
								<a href="#newsubcatmodal" class="serviceanchor center-align text-cedus-bluegray urbanservicename">
									<i class="material-icons">add</i>
									<br/>
									<span><i18n:message value="new_dashboard"/></span>
								</a>
							</div>
						</c:if>
					</div>
				
			</div>
		</div>

	</div>
	<%@include file="../frames/urbanservice_newmodal.jspf"%>
	
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/main.js"></script> 
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/urbanservices.js"></script>
	
	<%@include file="../frames/messages.jspf"%>
	
</body>

</html>
