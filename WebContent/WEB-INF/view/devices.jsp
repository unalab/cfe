<%@page import="it.eng.iot.configuration.Conf"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms"> 
 	<c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'technicalmanager')}">
 		<c:set var = "isTechnicalManager" scope = "session" value = "true"/> 
 	</c:if>
 	 <c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, '/spagobi/dev')}">
 		<c:set var = "isTechnicalManager" scope = "session" value = "true"/> 
 	</c:if>
</c:forEach> 



<html class="iotmanager">
<head>
	<title>City Enabler Front End</title>
	
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
	<!-- <script type="text/javascript" src="js/spagobiSDK/sbisdk-all-production.js"></script>  -->
	<script type="text/javascript" src="js/knowageSDK/sbisdk-all-production.js"></script>
	<script type="text/javascript" src="js/conf/knowage_conf.js"></script>
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/devices.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body class="materialize cedus-gray">
	<%@include file="../frames/loader.jspf"%>
	
	<%@include file="../frames/navbar.jspf"%>
	<!-- %@include file="../frames/index_actionbar.jspf" %>  -->
	<c:if test = "${fn:containsIgnoreCase(urbanServicePermission, 'C')}">
	 	<a href="#cockpitlinkmodal" class="btn-floating btn-large waves-effect waves-dark white" id="cockpit-link"><i class="material-icons">add</i></a>
	 	<a href="#cockpitselectmodal" class="btn-floating btn-large waves-effect waves-dark white" id="change-cockpit-link"><i class="material-icons">autorenew</i></a>
	 	<a href="#cockpitremove" class="btn-floating btn-large waves-effect waves-dark white" id="remove-cockpit-link"><i class="material-icons">close</i></a>
	</c:if>
	<div>
		<div class="row hide" id="errorMessage"></div>
		<div class="container row h-40">
			<div class="col s2 m1">
				<a href="urbanservices?scope=" id="backlink" class="btn-floating btn-flat waves-effect waves-light left">
					<i class="text-cedus-bluegray material-icons text-cedus-darkblue">arrow_back</i>
				</a>
			</div>
			<div class="col s10 m11 breadcrumb_bar nav-wrapper valign-wrapper">
		       <a href="#!" id="scope_breadcrumb" class="valign breadcrumb text-cedus-bluegray"></a>
		       <a href="#!" id="urbanservice_breadcrumb" class="valign breadcrumb text-cedus-bluegray"></a>
		       <a href="#!" id="dashboard_breadcrumb" class="valign breadcrumb text-cedus-bluegray"></a>
			</div>
		</div>
		<div id="listvisualization">
		<!-- 
			<div class="row">
				<ul class="pagination center">
					<li class="disabled"><a href="#!" class="navigation prev"><i class="material-icons">chevron_left</i></a></li>
					<li class="pageanchor template hide waves-effect"><a data-n="" href="#!"></a></li>
					<li class="waves-effect"><a href="#!" class="navigation next"><i class="material-icons">chevron_right</i></a></li>
				</ul>
			</div>
			 -->
			
			<div id="devicelist">
				<script type="text/javascript">
					currentLanguage = '<%=request.getSession().getAttribute("lang")%>';
				</script>
				<%@include file="../frames/index_deviceitem.jspf" %>
				<div class="row center-align" id="no-cockpit-linked">
					<h2><i18n:message value="no_cockpit_linked"/></h2>
				</div>
	
			</div>
		</div>
	
		
	</div>
	
	
	
		
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/devices.js"></script>
	
<!-- 	<script type="text/javascript">
		$(document).ready(function(){
			$('#iframedashboard').attr("src", "https://cedus.carto.com/u/antwerp/builder/"+get('dashboardid')+"/embed");
		});
	</script> -->
	
	<%@include file="../frames/messages.jspf"%>
	<%@include file="../frames/dashboard_newmodal.jspf"%>
	<%@include file="../frames/dashboard_select.jspf" %>

</body>

</html>