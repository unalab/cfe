$(document).ready(function(){
		
	onDocReady();
	
});

var checkedall = false;
var deviceperpage = 6;
var currpage = 1;
var urbanserviceslist = new Object();
var usUsed = new Object();
var serviceUsed = new Object();
function onDocReady(){
	//$('#knowage-loader').on('load', function() {
		mainOnDocReady();
		
		$('.modal').modal({dismissible:false});
		var scope = get('scope');
		
		// Get the refScope name
		getServiceName(scope);
		
		//Populate the scopelist through REST API invocation
		try{ 
			var input = new Object();
			input['action'] = 'getServicePaths';
			input['service'] = scope; 
	
			$.ajax({
				url: contextRoot + '/ajaxhandler',
				type : 'GET',
				dataType : 'json',
				data : input,
				success : function(data) {
					$.each(data, function(i, e){
					
						var queryparams = new Object();
							queryparams['scope'] = scope;
							queryparams['urbanservice'] = e.id;
							queryparams['dashboardid'] = e.dashboardid.value;
							
						var querystring = $.param(queryparams);
						
						appendItems(e, 'dashboard?'+querystring);
						usUsed[e] = true;
						serviceUsed[e.name.value] = true;
										
					});
					
					$('.delsubcat').removeClass("hide");
					$.each(uslist, function(i, e){
						if(typeof usUsed[e] === 'undefined' || !usUsed[e]){
							$('#newurbanservicetrigger').removeClass('hide');
						}
					});
					
					//appendSelectOption(usUsed);
					appendSelectOption(serviceUsed);
					//getDashboardList(Knowage.username,Knowage.password);
					
					$('#pageloader').fadeOut();
				},
				error : function(xhr, status, error) {
					console.log(error);
				}
			});
		}
		catch(error){
			console.log(error);
		}
	
//	});
};

function appendItems(value, url){

	if(typeof value.id === 'undefined' || value.id == null || value.id == "" || value.id == "/"){
		alert("ERROR VALUE: "+value);
	    return;
	}
		var template = $('#urbanserviceslist').find('.urbanserviceitem.template');
		var item = template.clone();
		item.removeClass('template hide');
		
		item.addClass(value.name.value);
		
		item.find('.block').css("background-image","url('images/icons/"+value.name.value.toLowerCase()+".png')");
		
		
		item.find(".urbanservicename").text(value.name.value);
		item.find(".serviceanchor").attr("href", url);
		item.append('<input type="hidden" name="us_id" value=' + value.id + ' />');
		
		var appender = $('#urbanserviceslist .row #newurbanservicetrigger');
		
		appender.before(item);

}

function appendNewItems(value, url){
	console.log("Value:" + value);
	
	if(typeof value === 'undefined' || value == null || value == "" || value == "/"){
		alert("ERROR VALUE: "+value);
	    return;
	}
		var template = $('#urbanserviceslist').find('.urbanserviceitem.template');
		var item = template.clone();
		var id = String(get('scope')) + "_" + String(value);
		item.removeClass('template hide');
		
		item.addClass(value);
		
		item.find('.block').css("background-image","url('images/icons/"+String(value).toLowerCase()+".png')");
		
		
		item.find(".urbanservicename").text(value);
		item.find(".serviceanchor").attr("href", url);
		item.append('<input type="hidden" name="us_id" value=' + id + ' />');
		
		var appender = $('#urbanserviceslist .row #newurbanservicetrigger');
		
		appender.before(item);

}

function appendSelectOption(usUsed){
	uslist.forEach(function(e){
		var us = usUsed[e];
		if(typeof us === "undefined" || us == null || !us){
			 $('#subcategorynamefield').append($('<option>', {
			    value: e,
			    text: e
			 }));
		}
	});
}

function getDashboardList(user,pass) {
		var input = new Object();
		var knowageUrl = Knowage.protocol + '://' 
						+Knowage.host + '/knowage/restful-services/2.0/documents';
		
		input["action"] = 'getAllDocuments';
		input["username"] = user;
		input["password"] = pass;
		
		if(Knowage.auth === "basic") {
			
			$.ajax({
				url: contextRoot + '/ajaxhandler',
			    type: 'POST',
			    dataType: 'json',
			    data: input,
				success: function(data) {
					appendDashboard(data)
				},
			    error: function(xhr, status, error) {
			    	alert(error);
			    	console.error(error);
			     }
			});
		
		} else {
			$.ajax({
				url: knowageUrl,
			    type: 'GET',
			    dataType: 'json',
				success: function(data) {
					appendDashboard(data)
				},
			    error: function(xhr, status, error) {
			    	alert(error);
			    	console.error(error);
			    }
			});
		}
}

function appendDashboard(data) {
	data.forEach(function(d){
		var dashboard = d.name;		
		 $('#documentnamefield').append($('<option>', {
		    value: dashboard,
		    text: dashboard
	    }));
		
	});
}


function registerUrbanService(scope, service, cockpitName, success_callback) {
	var headersAll = new Object();
		headersAll['fiware-service'] = scope;
		headersAll['fiware-servicepath'] = service;
					
	var input = new Object();
		input["action"] = 'create_urban_service';
		if(cockpitName.trim().length > 0) {
			input["cockpitname"] = cockpitName;
		}
		
	$.ajax({
		url: contextRoot + '/ajaxhandler',
	    type: 'POST',
	    dataType: 'json',
	    data: input,
		headers: headersAll,
		success: success_callback,
	    error: function(xhr, status, error) {
	    	alert(error);
	    	console.error(error);
	     }
	});
}

function deleteDashboard(serv, path, id, success_callback){
	
	var input = new Object();
		input['action'] = 'deleteDashboard';
		input['scope'] = serv;
		input['dashboard'] = path;
		
	var headersAll = new Object();
		

		
	$.ajax({
		url: contextRoot + '/ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : success_callback,
		error : function(xhr, status, error) {
			alert(error);
			console.error(error);
		}
	});
}

$(document).on('click', '#newurbanservicebutton', function(){
//	var selectedOption = $('#subcategorynamefield').find('option:selected');
//	var selectedCockpit = $('#documentnamefield').find('option:selected');
//	$('#selectedurbanservice').val(selectedOption.val());
//	$('#selecteddashboard').val(selectedCockpit.val());
//	
//	selectedOption.remove();
	
	var selectedOption = $('#subcategorynamefield').find('option:selected');
	$('#selectedurbanservice').val(selectedOption.val());
	selectedOption.remove();
});

$(document).on('submit', '#newsubcatform', function(){
	
//	var path = $(this).find('#selectedurbanservice').val();
//	$(this).find('#selectedurbanservice').val("");
//	var cleanpath = path.replace(/\s\s+/g, ' ').trim().replace(new RegExp(" ", 'g'), "_");
//	//var cockpitName = $(this).find('#cockpitname').val().replace(/\s\s+/g, ' ').trim().replace(new RegExp(" ", 'g'), "_");
//	var cockpitName = $(this).find('#selecteddashboard').val();
//	$(this).find('#selecteddashboard').val("");
	
	var path = $(this).find('#selectedurbanservice').val();
	$(this).find('#selectedurbanservice').val("");
	var cleanpath = path.replace(/\s\s+/g, ' ').trim().replace(new RegExp(" ", 'g'), "_");
	var cockpitName = $(this).find('#cockpitname').val().replace(/\s\s+/g, ' ').trim().replace(new RegExp(" ", 'g'), "_");
	
	var serv = get('scope');
	console.log("COCKPITNAME: "+cockpitName);
	var queryparams = new Object();
		queryparams['scope'] = serv;
		queryparams['urbanservice'] = serv + "_"+ cleanpath;
		queryparams['dashboardid'] = cockpitName;
		
	var querystring = $.param(queryparams);
	
	registerUrbanService(serv, cleanpath, cockpitName, function() {
		appendNewItems(cleanpath, "dashboard?"+querystring);
		
		usUsed[cleanpath]  = true;
		var full = true;
		$.each(uslist, function(i, e){
			if(typeof usUsed[e] === 'undefined' || !usUsed[e]){
				full = false;
			}
		});
		if(full){
			$('#newurbanservicetrigger').addClass('hide');
		}
	});

	
	$('.modal#newsubcatmodal').modal('close');
	$(this).find('#cockpitname').val("");
	return false;
});

$(document).on('click', '.delsubcat', function(){
	if(confirm($('#msg_confirm').text())){
		var self = $(this);
		var serv = get('scope'); 
		var path = self.closest('.urbanserviceitem ').find('.urbanservicename').text();
		var id = $("input[name=us_id]").val();
		
		deleteDashboard(serv, path, id, function(data){		
			self.closest('.urbanserviceitem ').remove();
			self.closest('.catitem').remove();
			delete usUsed[path];
			$('#newurbanservicetrigger').removeClass('hide');
		});
		
		$('#subcategorynamefield').append($('<option>', {
		    value: path,
		    text: path
		 }));
			
	};
});


