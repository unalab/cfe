$(document).ready(function(){
	
	$('#knowage-loader').on('load', function() {
		mainOnDocReady();	
		$('.modal').modal({dismissible: false});
		
		 //Populate the scopelist through REST API invocation
		try{ 
			var input = new Object();
			input['action'] = 'getServices';
	
			$.ajax({
				url: contextRoot + '/ajaxhandler',
				type : 'GET',
				dataType : 'json',
				data : input,
				success : function(data) {
					$('#nrcat').find('span').text(data.length);
					
					$.each(data, function(i, e){
						var queryparams = new Object();
							queryparams['scope'] = e.id;
						
						var querystring = $.param(queryparams);
						
						appendItems(e.name.value, 'urbanservices?'+querystring);
					
					});
					
					$('#pageloader').fadeOut();
					
				},
				error : function(xhr, status, error) {
					console.log(error);
				}
			});
		}
		catch(error){
			console.log(error);
		}
	});
});

function appendItems(value, url){

	var template = $('#catlist').find('.catitem.template');
	var item = template.clone();
	item.removeClass('template hide');
	
	
	item.find(".catname").text(value);
	item.find(".catanchor").attr("href", url);
	var appender = $('#catlist .row #newcattrigger');
	
	appender.before(item);

}

function registerScope(service, servicepath, success_callback, error_callback) {
	
	var headersAll = new Object();
		headersAll['fiware-service'] = service;
		headersAll['fiware-servicepath'] = servicepath;
		
		
	var input = new Object();
		input["action"] = 'create_scope';
		input["scope"] = service;

		
	$.ajax({
		url: contextRoot + '/ajaxhandler',
	    type: 'POST',
	    dataType: 'json',
	    data: input,
		headers: headersAll,
		success: success_callback,
	    error: error_callback
	});
}


function deleteScope(serv, success_callback){

	var input = new Object();
		input['action'] = 'deleteScope';
		input['scope'] = serv;
		
	var headersAll = new Object();
		headersAll['fiware-service'] = serv;

	$.ajax({
		url: contextRoot + '/ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : success_callback,
		error : function(xhr, status, error) {
			alert(error);
			console.error(error);
		}
	});
}


$(document).on('submit', '#newcatform', function(){
	var form = $(this);
	var exists = false;
	
	var newcatname = form.find('#categorynamefield').val();
	var cleanCatName = newcatname.replace(/\s\s+/g, ' ').trim().replace(new RegExp(" ", 'g'), "_");
	cleanCatName = String(cleanCatName).toLowerCase();
	
	//Check if the scope already exists
	$('.catitem').each(function(cat) {
		if($(this).find(".catname").text().toLowerCase() == cleanCatName) {
			exists = true;
			alert("The city already exists");
			return false;
		}		
	});	
	if(exists) return false;
	
	form.find('.modal-footer input').prop('disabled', true);
	registerScope(cleanCatName, "", function(data){
			
			var queryparams = new Object();
		    queryparams['scope'] = cleanCatName; 
		   
		    var querystring = $.param(queryparams);
			
			appendItems(cleanCatName, 'urbanservices?'+querystring);
			
			$('.modal#newcatmodal').modal('close');
			form.find('#categorynamefield').val("");
			form.find('.modal-footer input').prop('disabled', false);
		}, 
		function(xhr, status, error){
	    	console.error(error);
			form.find('.modal-footer input').prop('disabled', false);
			alert(error);
		});
	
	return false;
	
});

$(document).on('click', '.delcat', function(){
	if(confirm($('#msg_confirm').text())){
		var self = $(this);
		var domitem = self.closest('.catitem').find('.catname').text();
		
		var input = new Object();
		input['action'] = 'deleteDashboardFromScopeName';
		input['scope'] = domitem; 
		
		console.log("DOMITEM: "+domitem);
		
		//Remove all the dashboard associated with the scope
		$.ajax({
			url: contextRoot + '/ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				deleteScope(domitem,function(){});
				self.closest('.catitem').remove();
			},
			error : function(xhr, status, error) {
				alert(error);
				console.log(error);
			}
		});
	}
});
