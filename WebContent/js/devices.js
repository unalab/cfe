var checkedall = false;
var deviceperpage = 6;
var currpage = 1;
var devicelist = new Object();
var mapcenter = {};
var scope = '';
var urbanservice = '';
var cockpitNameGlobal = '';
var cockpitListGlobal = [];

$(document).ready(function(){
	
	onDocReady();
	
	// Get the refScope name
	var scopeId = get('scope');
	getServiceName(scopeId);
	var urbanserviceId = get('urbanservice');
	getServicePathName(scopeId, urbanserviceId);

});
	

function onDocReady(){
	mainOnDocReady();
	//invokeKnowage('empty_login');
	$('.modal').modal({dismissible:false});
	$('body').removeClass('cedus-gray');
	$('body').addClass('knowage-gray');
	scope = get('scope');
	urbanservice = get('urbanservice');
	
	$('#scope_breadcrumb').text(scope).attr("href", 'index');
	$('#urbanservice_breadcrumb').text(urbanservice).attr("href", "urbanservices?scope="+scope);
	$('#dashboard_breadcrumb').text(cockpitNameGlobal).attr("href", "");
	$('#backlink').attr('href', 'urbanservices?scope='+scope);
	
	$('#scopeformval').val(scope);
	$('#urbanserviceformval').val(urbanservice);

	$('#pageloader').fadeOut();
	
	getDashboardList(Knowage.username,Knowage.password);
	
};

function getDashboardList(user,pass) {
	var input = new Object();
	input["action"] = 'getAllDocuments';
	input["username"] = user;
	input["password"] = pass;
	
//	$.ajax({
//		url: contextRoot + '/ajaxhandler',
//	    type: 'POST',
//	    dataType: 'json',
//	    data: input,
//		success: function(data) {
//			appendDashboard(data);
//			getCockpitName(get('urbanservice'));
//		},
//	    error: function(xhr, status, error) {
//	    	alert(error);
//	    	console.error(error);
//	     }
//	});
	
	getCockpitName(get('urbanservice'));
}

function appendDashboard(data) {
	data.forEach(function(d){
		var dashboard = d.name;		
		 $('#cockpitnamefield').append($('<option>', {
		    value: dashboard,
		    text: dashboard
	    }));
		
	});
}

function getCockpitName(urbanService) {
	var input = new Object();
	input['action'] = 'getCockpitNames';
	input['urbanService'] = urbanService; 
	
	
	$.ajax({
		url: contextRoot + '/ajaxhandler',
	    type: 'GET',
	    dataType: 'text',
	    data: input,
		success: function(cockpitNames) {
			var cNames = JSON.parse(cockpitNames);
			cockpitListGlobal = cNames;
			if(cNames.length == 0) {
				$(".deviceitem").addClass("hide"); 
				$("#remove-cockpit-link").addClass("hide");
				$("#change-cockpit-link").addClass("hide");
				$("#cockpit-link").css("bottom","30px");
			}
			else if(cNames.length == 1) {
				$("#change-cockpit-link").addClass("hide");
				cockpitNameGlobal = cNames[0];
				renderCockpit(cNames[0]);
			}
			else {
				cNames.forEach(function(d){
					var dashboard = d;		
					 $('#cockpitnames').append($('<option>', {
					    value: dashboard,
					    text: dashboard
				    }));
					
				});
				$('#cockpitselectmodal').modal('open'); 
			}
				
			
		},
	    error: function(xhr, status, error) {
	    	alert(error);
	    	console.error(error);
	    	return null;
	     }
	});
}

function renderCockpit(cockpitName) {
	console.log("Cockpit name: "+cockpitName);
	console.log("Cockpit name length: "+cockpitName.length);
	if(cockpitName!=null && cockpitName.trim().length != 0) {
		cockpitNameGlobal = cockpitName;
		//Hide No cockpit linked div
		$('#no-cockpit-linked').addClass("hide");
		//Pre fill cockpit name
		//$('#cockpitnamefield').val(cockpitName);
		//Remove hide class from iframe
		$('#iframedashboard').removeClass("hide");
		//Set iFrame Attribute 
		$('#dashboard_breadcrumb').text(cockpitNameGlobal).attr("href", "");
		invokeKnowage(cockpitName);		
	} else {
		$('.deviceitem').addClass("hide");
	}
}


$(document).on('submit','#cockpitnameform',function(){
	var cockpitName = $('#cockpitnamefield').val();
	if(cockpitName.trim().length > 0 && cockpitListGlobal.indexOf() < 0)
		setCockpitName(cockpitName);
	/*else {
		$('#cockpitnamefield').css("border-bottom-color", "red");
		$('#cockpitnamefield').css("box-shadow", "0 1px 0 0 red");
		return false;
	}
	*/
});


$(document).on('submit','#cockpitselectnameform',function(){
	var cockpitName = $('#cockpitnames').val();
	cockpitNameGlobal = cockpitName;
	renderCockpit(cockpitName);
});

$(document).on('click','.modal-close',function(){
	$('#cockpitnamefield').val(cockpitNameGlobal);
	$('#cockpitnamefield').css("border-bottom-color", "#9e9e9e");
	$('#cockpitnamefield').css("box-shadow", "none");
});

function removeDashboard(id, cockpitId, success_callback){
	
	var input = new Object();
		input['action'] = 'removeDashboard';
		input['entityId'] = id;
		input['cockpitName'] = cockpitId;
		
	var headersAll = new Object();
		

		
	$.ajax({
		url: contextRoot + '/ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : success_callback,
		error : function(xhr, status, error) {
			alert(error);
			console.error(error);
		}
	});
}

$('#remove-cockpit-link').click(function(){
	if(confirm($('#msg_confirm').text())){
		var id = get('urbanservice');
		var dashboardId = cockpitNameGlobal;
		
		removeDashboard(id,dashboardId, function(data) {
			location.reload();
		})
	}
});



//Add new cockpit
function setCockpitName(cockpitName) {
	var input = new Object();
	input['action'] = 'appendDashboard';
	input['cockpitName'] = cockpitName; 
	input['entityId'] = get('urbanservice');
	
	$.ajax({
		url: contextRoot + '/ajaxhandler',
	    type: 'POST',
	    dataType: 'json',
	    data: input,
		success: function(data) {
			location.reload();
		},
	    error: function(xhr, status, error) {
	    	alert(error);
	    	console.error(error);
	    	return null;
	     }
	});
}


function invokeKnowage(cockpitName) {
    Sbi.sdk.services.setBaseUrl({
        protocol: Knowage.protocol
        , host: Knowage.host
        , port: Knowage.port
        , contextPath: 'knowage'
        , controllerPath: 'servlet/AdapterHTTP'
    });    
    
    this.renderCockpitInFrame = function() {
	    var url = Sbi.sdk.api.getDocumentUrl({
			documentLabel: cockpitName//'MAP_TEST_DANILO'cockpitName
			, executionRole: 'technicalmanager'
			, displayToolbar: false
			, displaySliders: false
			, height: '1000px'
			, width: '1200px'
			, iframe: {
				style: 'border: 0px;'
			}
		});
	    
	    
	    document.getElementById('iframedashboard').src = url;
	};
	
    //Callback definition for basic auth
    var callback = function(result, args, success) {
      if(success) {
        this.renderCockpitInFrame();
      } else {
        alert("Connection problem!");
      }
    };
    
	switch (Knowage.auth) {
		case "idm":
			renderCockpitInFrame();
		break;
		
		case "basic":
		    //Knowage Authentication
		    Sbi.sdk.api.authenticate({
		  		params: {
		  			user: Knowage.username,
		  			password: Knowage.password
		  		},
		  		callback: {
		  			fn: callback,
		  			scope: this
		  		}
		  	});
		break;
	}





}


