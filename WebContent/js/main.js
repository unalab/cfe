/**
 * Get all the parameters from the url
 * 
 * @return [string] The vars
 */
function get(name) {
	 if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	      return decodeURIComponent(name[1]);
}

/**
 * Logout Go to the IDM login page
 */
/*
function logout() {
	delete code;
	window.location.replace(cityenablerUrl + auth_logout);
}
*/

function getServiceList() {

	var input = new Object();
	input['action'] = 'getContextsList';

	$.ajax({
		url : contextRoot + '/ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			console.log(data);
			appendCategories(data);
		},
		error : function(xhr, status, error) {
			console.error(error);
		}
	});
}

var currentLanguage;

function mainOnDocReady(){
	
	$(".activeLang").text(currentLanguage);
	
	$('.languagemenu').each(function(){
		var self = $(this);
		self.find('a').each(function(){
			$(this).attr('href', $(this).attr('href')+"&scope="+get('scope')+"&urbanservice="+get('urbanservice'));
		});
	});
	
	$("#"+currentLanguage).closest('a').attr('href', '#!');
	$("#"+currentLanguage).removeClass('hide');
	
	var devreg_success = get("success");
	if(devreg_success){
		setTimeout(function(){
			Materialize.toast(get("msg"), 4000, 'cedus-green'); 
		}, 1000);
		
	}

	$('select.materialselect').material_select();
	$(".button-collapse").sideNav();
	$(".dropdown-button").each(function() {
				var constrainWidth_p = true;
				constrainWidth_p = constrainWidth_p && $(this).attr('data-constrainwidth');
				$(this).dropdown({
					hover : false,
					constrainWidth : constrainWidth_p,
					alignment : 'right'
				});
			});
};

$(document).on('click', '#mobilesearch_trigger', function() {
	var searchbar = $('#mobilesearch');
	searchbar.is(":visible") ? searchbar.slideUp() : searchbar.slideDown();
	return false;
});

//SSH 
var session_token = "token";
var session_refreshToken = "refresh_token";
function SetLocalStorage (evt) {
      localStorage.removeItem(session_token);
      localStorage.removeItem(session_refreshToken);

}

if (window.addEventListener) {
// For standards-compliant web browsers
  window.addEventListener("message", SetLocalStorage, false);
}
else {
  window.attachEvent("onmessage", SetLocalStorage);
}

$(document).ready(function() {
    $('select').material_select();    
});

$(document).on('input change', '#search', function(){
	var input = $(this).val().trim();
	var close_icon = $(this).closest('form').find('.closeicon');
	
	if(input.length>0){ close_icon.removeClass('hide'); }
	else{ close_icon.addClass('hide'); }
});

$(document).on('change', '#search', function(){
	var trimmed = $(this).val().trim();
	$(this).val(trimmed);
});

$(document).on('click', '.closeicon', function(){
	$(this).closest('form').find('#search').val("").change();
});


