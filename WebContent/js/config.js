
/* CONFIGURE IN CASE THE APPLICATION IS DEPLOYED ON THE SERVER PROD or TEST ENV */
var productionEnv = false;
//var productionEnv = true;

/* Configure landing */
//PROD ENV 			
//var redirect_uri = "http://217.172.12.202/landing";
var redirect_uri = "http://unalab.eng.it/landing";

/* Configure origin_page */

var origin_page = "localhost:8080/cfe/login";

//var origin_page = "http://unalab.eng.it/login"

/* Configure contextRoot */

//LOCALHOST ENV		
var contextRoot = "/cfe";

/* Configure client_id */
var client_id = "134f3121b7e04a1994c15b81525ca3c1";

/* **************************************** */
/* IDM Configuration 						*/
/* **************************************** */
var auth_logout = "/logout";

/* Configure idmURL */

//var idmURL = "http://217.172.12.202:8000";
var idmURL = "http://idm.unalab.eng.it";

/* **************************************** */
/* Context Broker Configuration 			*/
/* *************************************** */
/* PROD ENV 
var cbHost = "https://cb.s4c.eng.it";
var cbHostRemote = "http://192.168.150.29:1026";
var cbNotifyHost = "http://192.168.150.29:5050/notify";
*/
/* TEST ENV */
/*
var cbHost = "http://192.168.111.204:1026";
var cbHostRemote = "http://192.168.111.204:1026";
var cbNotifyHost = "http://192.168.111.204:5050/notify";
*/
/* LOCALHOST ENV
var cbHost = "https://cb.s4c.eng.it";
var cbHostRemote = "http://217.172.12.145:1026";
var cbNotifyHost = "http://217.172.12.145:5050/notify";
*/


// Trento Location
var malagacoords = { lat: 46.0747793, lng: 11.1217486 };

/* Business configuration */
var uslist = ["Parking", "Waste", "Mobility", "Illumination", "Environment", "Tourism", "Water", "KPI", "Issues", "Taxes","Weather"];




